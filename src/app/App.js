import React, { Component } from 'react';
import { render } from 'react-dom';

class App extends Component {
    constructor() {
        super();
        this.state = {
            title: '',
            description: '',
            tasks: [],
            id: ''
        }
        this.addTask = this.addTask.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        this.fetchTasks();
    }

    addTask(e) {
        e.preventDefault();
        console.log(this.state);
        if (this.state.id) {
            fetch(`/api/tasks/${this.state.id}`, {
                method: 'PUT',
                body: JSON.stringify(this.state),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
                .then(res => res.json())
                .then(data => {
                    console.log(data)
                    M.toast({ html: 'Tarea actualizada' })
                    this.setState({
                        title: '',
                        description: '',
                        id: ''
                    })
                    this.fetchTasks();
                })
        } else {
            fetch('/api/tasks', {
                method: 'POST',
                body: JSON.stringify(this.state),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
                .then(res => res.json())
                .then(data => {
                    console.log(data)
                    M.toast({ html: 'Tarea guardada' })
                    this.setState({
                        title: '',
                        description: ''
                    })
                    this.fetchTasks();
                })
                .catch(err => console.log(err))
        }
    }


    fetchTasks() {
        fetch('/api/tasks')
            .then(res => res.json())
            .then(data => {
                this.setState({ tasks: data });
                console.log(this.state.tasks)
            });
    }

    deleteTask(id) {
        // fetch('/api/tasks/' + id)
        if (confirm('Está seguro de eliminar el elemento')) {
            fetch(`/api/tasks/${id}`, {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            }).then(res => res.json())
                .then(data => {
                    console.log(data);
                    M.toast({ html: 'Tarea eliminada' })
                    this.fetchTasks();
                });
        }
    }

    editTask(id) {
        fetch(`/api/tasks/${id}`)
            .then(res => res.json())
            .then(data => {
                console.log(data);
                this.setState({
                    title: data.title,
                    description: data.description,
                    id: data._id
                })
            });

    }
    handleChange(e) {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        })

    }
    render() {
        return (
            <div>
                {/*NAVIGATION */}
                <nav className='light-blue darken-4'>
                    <div className='container'>
                        <a className='brand-logo' href='/'>
                            MERN STACK
                        </a>
                    </div>
                </nav>

                <div className='container'>
                    <div className='row'>
                        <div className='col s5'>
                            <div className='card'>
                                <div className='card-content'>
                                    <form onSubmit={this.addTask}>
                                        <div className='row'>
                                            <div className='input-field col s12'>
                                                <input type='text'
                                                    placeholder='Titulo tarea'
                                                    name='title'
                                                    onChange={this.handleChange}
                                                    value={this.state.title} />
                                            </div>
                                        </div>
                                        <div className='row'>
                                            <div className='input-field col s12'>
                                                <textarea placeholder='Descripción tarea'
                                                    className='materialize-textarea'
                                                    name='description'
                                                    onChange={this.handleChange}
                                                    value={this.state.description}></textarea>
                                            </div>
                                        </div>
                                        <button className='btn light-blue darken-4' type='submit'>Enviar</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div className='col s7'>
                            <table>
                                <thead>
                                    <tr>
                                        <th>Titulo</th>
                                        <th>Descripción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        this.state.tasks.map(task => {
                                            return (
                                                <tr key={task._id}>
                                                    <td>{task.title}</td>
                                                    <td>{task.description}</td>
                                                    <td>
                                                        <button className='btn light-blue darken-4'
                                                            style={{ margin: '4px' }}
                                                            onClick={() => this.editTask(task._id)}>
                                                            <i className='material-icons'>edit</i>
                                                        </button>
                                                        <button className='btn light-blue darken-4'
                                                            onClick={() => this.deleteTask(task._id)}>
                                                            <i className='material-icons'>delete</i>
                                                        </button>
                                                    </td>
                                                </tr>
                                            )
                                        })
                                    }
                                </tbody>
                            </table>

                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

export default App;