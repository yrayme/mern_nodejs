const express = require('express');
const morgan = require('morgan');
const path = require('path');

const {mongoose} =require('./dataBase');

const app = express();

//Setting
app.set('port', process.env.PORT || 4000)

//Middlewares
app.use(morgan('dev'));
app.use(express.json());

//Routes
app.use('/api/tasks', require('./routes/task_routes'));

//Static files
// console.log(__dirname + '/public'); //Da un a dirección de donde esta el archivo
// console.log(path.join(__dirname, 'public'));
app.use(express.static(path.join(__dirname, 'public')));

//Starting the server


app.listen(app.get('port'), () => {
    console.log(`Server in port ${app.get('port')}`)
});
